#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char word[PASS_LEN];
    char hashes[HASH_LEN];
};

int by_hash(const void *a, const void *b)
{
    struct entry *aa = (struct entry *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa->hashes, bb->hashes);
} 

int search_by_hash(const void *t, const void *e)
{
    return strcmp((char*)t,((struct entry*)e)->hashes);
}


struct entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    int lines =50;
    struct entry * entries =malloc(lines *sizeof(struct entry));
    FILE *d = fopen(filename, "r");
    char line[100];
    int count =0;
    while(fgets(line,100,d)!=NULL)
    {
        if(count ==lines)
        {
            lines+=50;
            entries = realloc(entries, lines*sizeof(struct entry));
        }
        
        line[strlen(line)-1] = '\0';
        strcpy(entries[count].word,line);
        char *hash =md5(line,strlen(line));
        strcpy(entries[count].hashes,hash);
        free(hash);
        count++;
    }
    *size = count;
    fclose(d);
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    //Read the dictionary file into an array of entry structures
    int dlen =0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    qsort(dict, dlen, sizeof(struct entry), by_hash);

    // Open the hash file for reading.
    FILE *f =fopen(argv[1], "r");

    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.

    char line [HASH_LEN];
    while(fgets(line, HASH_LEN,f)!=NULL)
    {
        line[HASH_LEN -1] = '\0';
        for(int i=0;i<dlen;i++)
        {
            struct entry *found = bsearch(line, dict, dlen, sizeof(struct entry), search_by_hash);
            if(found)
            {
                printf("%s: cracked to -> %s\n", found ->hashes, found->word);
                break;
            }
        }
    }
    fclose(f);
    free(dict);
}
